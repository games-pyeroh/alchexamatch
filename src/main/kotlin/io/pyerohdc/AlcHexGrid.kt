package io.pyerohdc

import javafx.geometry.Point3D

class AlcHexGrid(
    val nextMetalMarble: Marble.MetalMarbleOrder?,
    states: Map<Point3D, MarbleState>
) : HexGrid<MarbleState>(updateMarblesStates(nextMetalMarble, states)) {

    companion object {
        val initialMarbles = mapOf(
            Marble.SALT to 4,
            Marble.WATER to 8,
            Marble.FIRE to 8,
            Marble.AIR to 8,
            Marble.EARTH to 8,
            Marble.DEATH to 4,
            Marble.LIFE to 4,
            Marble.QUICKSILVER to 5,
            Marble.LEAD to 1,
            Marble.TIN to 1,
            Marble.IRON to 1,
            Marble.COPPER to 1,
            Marble.SILVER to 1,
            Marble.GOLD to 1
        )

        private const val sideSize = 6

        fun random(): AlcHexGrid {
            val marbles = mutableMapOf<Point3D, Marble>()

            val points = allPointsForSize(sideSize).toMutableList()

            marbles[Point3D(.0, .0, .0)] = Marble.GOLD
            points.remove(Point3D(.0, .0, .0))

            marbles += initialMarbles
                .filterKeys { it != Marble.GOLD }
                .flatMap { e -> List(e.value) { e.key } }
                .associateBy { points.random().also { points.remove(it) } }

            return AlcHexGrid(marbles)
        }

        private fun marblesToStates(
            nextMetalMarble: Marble.MetalMarbleOrder?,
            marbles: Map<Point3D, Marble>
        ): Map<Point3D, MarbleState> {
            val neighbourDirections = neighbourDirections()

            return marbles.map { (point, marble) ->
                val neighbours = neighbourDirections
                    .map { point.add(it) }
                    .map { marbles[it] }

                val active = neighbours.windowed(3, partialWindows = false).any { it.filterNotNull().isEmpty() }
                        || neighbours.rotate(2).windowed(3, partialWindows = false).any { it.filterNotNull().isEmpty() }
                val metalActive = !marble.metal || nextMetalMarble == null || nextMetalMarble.marble == marble
                point to MarbleState(marble, active && metalActive)
            }
                .toMap()
        }

        private fun updateMarblesStates(
            nextMetalMarble: Marble.MetalMarbleOrder?,
            marblesStates: Map<Point3D, MarbleState>
        ): Map<Point3D, MarbleState> {
            val neighbourDirections = neighbourDirections()

            return marblesStates.map { (point, state) ->
                if (state.removed) {
                    return@map point to state.copy(active = false)
                }

                val neighbours = neighbourDirections
                    .map { point.add(it) }
                    .map { marblesStates[it] }

                val active = neighbours
                    .windowed(3, partialWindows = false)
                    .any { it.filterNotNull().none { m -> !m.removed } }
                        || neighbours.rotate(2)
                    .windowed(3, partialWindows = false)
                    .any { it.filterNotNull().none { m -> !m.removed } }
                val metalActive =
                    !state.marble.metal || nextMetalMarble == null || nextMetalMarble.marble == state.marble
                point to state.copy(active = active && metalActive)
            }
                .toMap()
        }

        private fun neighbourDirections(): List<Point3D> {
            return listOf(
                Point3D(0.0, 1.0, -1.0),
                Point3D(1.0, 0.0, -1.0),
                Point3D(1.0, -1.0, 0.0),
                Point3D(0.0, -1.0, 1.0),
                Point3D(-1.0, 0.0, 1.0),
                Point3D(-1.0, 1.0, 0.0)
            )
        }
    }

    private val _selected: MutableList<Point3D> = mutableListOf()
    val selected: List<Point3D>
        get() = _selected.toList()

    val won: Boolean
        get() = items.all { it.value.removed }

    constructor(marbles: Map<Point3D, Marble>) : this(
        Marble.MetalMarbleOrder.first(),
        marblesToStates(Marble.MetalMarbleOrder.first(), marbles),
    ) {
        check(marbles.values.groupingBy { it }
            .eachCount() == initialMarbles) { "Incorrect number of marbles : $marbles" }
    }

    /**
     * Add selected marble to list of selected marbles in grid.
     */
    // selection is not stored in MarbleState because it would be saved (when the grid will be stored to the disk), and I don't care about selection
    fun select(point: Point3D): AlcHexGrid? {
        val state = items[point]
        check(state != null && !state.removed) { "You can only select an existing position" }
        check(state.active) { "You can only select an active marble" }

        return if (point in _selected) {
            // if already selected, remove selection
            _selected.remove(point)
            null
        } else if (state.marble == Marble.GOLD) {
            _selected.clear()
            // if gold is selected, nothing to register, output an updated grid
            val states = items.toMutableMap()
            states[point] = state.copy(removed = true)

            AlcHexGrid(null, states)
        } else if (_selected.isEmpty()) {
            // if first selection, nothing special, just update "selected" list
            _selected.add(point)
            null
        } else {
            // if second selection, check if it matches with first selection
            val firstSelected = _selected.first()
            val firstState = items[firstSelected]!!

            if (state.marble.canBePairedWith(firstState.marble)) {
                _selected.clear()
                // if it matches, "remove" theses marbles, and output an updated grid
                val pointsToRemove = listOf(firstSelected, point)
                val states = items.toMutableMap()
                pointsToRemove.forEach { states[it] = states[it]!!.copy(removed = true) }

                val nextMetalMarble =
                    if (listOf(firstState, state).any { it.marble.metal }) nextMetalMarble?.next() else nextMetalMarble
                AlcHexGrid(nextMetalMarble, states)
            } else {
                // if it doesn't match, swap selection
                _selected[0] = point
                null
            }
        }
    }

    fun neighbours(point: Point3D): Map<Point3D, MarbleState?> {
        return neighbourDirections()
            .map { point.add(it) }
            .associateWith { items[it] }
            .mapValues {
                val state = it.value
                if (state != null && state.removed) null else state
            }
    }
}

enum class Marble(private val symbol: String, val metal: Boolean = false) {
    EARTH("🜃"),
    AIR("🜁"),
    FIRE("🜂"),
    WATER("🜄"),
    SALT("🜔"),
    DEATH("🜞"),
    LIFE("🜍"),

    QUICKSILVER("☿"),
    LEAD("♄", true),
    TIN("♃", true),
    IRON("♂", true),
    COPPER("♀", true),
    SILVER("☽", true),
    GOLD("☉", true)
    ;

    fun canBePairedWith(marble: Marble): Boolean {
        return marble in canBePairedWith()
    }

    private fun canBePairedWith(): Set<Marble> {
        return when (this) {
            EARTH, AIR, FIRE, WATER -> setOf(this, SALT)
            SALT -> setOf(EARTH, AIR, FIRE, WATER, SALT)
            DEATH -> setOf(LIFE)
            LIFE -> setOf(DEATH)
            QUICKSILVER -> setOf(LEAD, TIN, IRON, COPPER, SILVER)
            LEAD, TIN, IRON, COPPER, SILVER -> setOf(QUICKSILVER)
            GOLD -> emptySet()
        }
    }

    override fun toString(): String {
        return symbol
    }

    enum class MetalMarbleOrder(
        val marble: Marble,
        val order: Int
    ) {
        LEAD(Marble.LEAD, 1),
        TIN(Marble.TIN, 2),
        IRON(Marble.IRON, 3),
        COPPER(Marble.COPPER, 4),
        SILVER(Marble.SILVER, 5),
        GOLD(Marble.GOLD, 6);

        fun next() = values().filter { it.order > this.order }.minByOrNull { it.order }

        companion object {
            fun first() = values().minByOrNull { it.order }!!
        }
    }
}

data class MarbleState(
    val marble: Marble,
    val active: Boolean,
    val removed: Boolean = false
) {
    override fun toString(): String {
        return if (removed) "_" else marble.toString()
    }
}

