package io.pyerohdc

import javafx.application.Application
import javafx.application.Application.launch
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

class Main : Application() {
    override fun start(primaryStage: Stage) {
        val root = FXMLLoader.load<Parent>(javaClass.classLoader.getResource("sample.fxml"))
        primaryStage.title = "Hello World"
        primaryStage.scene = Scene(root/*, 835.0, 868.0*/)
        primaryStage.sizeToScene()
        primaryStage.show()
    }

}

fun main(args: Array<String>) {
    launch(Main::class.java, *args)
}
