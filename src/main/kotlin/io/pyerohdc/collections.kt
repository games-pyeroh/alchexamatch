package io.pyerohdc

import java.util.*

fun <T> List<T>.rotate(n: Int): List<T> {
    if (n == size || n == 0) {
        return this
    }

    val mut = this.toMutableList()
    Collections.rotate(mut, n)
    return mut.toList()
}

fun <K, V> Pair<K, V>.toEntry() = object : Map.Entry<K, V> {
    override val key: K = first
    override val value: V = second
}
