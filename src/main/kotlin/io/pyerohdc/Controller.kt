package io.pyerohdc

import javafx.concurrent.Task
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Alert
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.StackPane
import java.net.URL
import java.time.Instant
import java.util.*

class Controller : Initializable {

    @FXML
    private lateinit var gridWrapper: AnchorPane

    private lateinit var grid: HexGrid<StackPane>

    private lateinit var alcHexGrid: AlcHexGrid
    private lateinit var alcHexGridOrigin: AlcHexGrid

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        val panes = gridWrapper.children.filterIsInstance<StackPane>()
        grid =
            HexGrid(HexGrid.allPointsForSize(HexGrid.sideSizeFromCollectionSize(panes.size)).zip(panes).toMap())

        newGame()
    }

    @FXML
    fun newGame() {
        alcHexGrid = AlcHexGrid.random()
        alcHexGridOrigin = alcHexGrid
        bindAlchexGrid(alcHexGrid)
    }

    @FXML
    fun restart() {
        alcHexGrid = alcHexGridOrigin
        bindAlchexGrid(alcHexGrid)
    }

    @FXML
    fun isSolvable() {
        val task = object : Task<Unit>() {
            override fun call() {
                println("start solvable : ${Instant.now()}")
                println("solvable ? ${Solver(alcHexGrid).isSolvable()}")
                println(Instant.now())
            }
        }

        task.setOnFailed { it.source.exception.printStackTrace() }

        Thread(task).start()
    }

    private fun bindAlchexGrid(alcHexGrid: AlcHexGrid) {
        grid.items.forEach { (_, pane) ->
            pane.children.clear()
            pane.setOnMouseEntered { }
            pane.setOnMouseExited { }
            pane.setOnMouseClicked { }
        }

        alcHexGrid.items
            .filterValues { !it.removed }
            .forEach { (point, marbleState) ->
                val pane = grid.items.getValue(point)
                pane.setOnMouseEntered { pane.setImage(hoverMarbleImages.getValue(marbleState.marble), marbleState) }
                pane.setOnMouseExited { pane.setImage(marbleImages.getValue(marbleState.marble), marbleState) }
                if (marbleState.active) {
                    pane.setOnMouseClicked { _ ->
                        val nextGrid = alcHexGrid.select(point)
                        if (nextGrid != null) {
                            bindAlchexGrid(nextGrid)
                        } else {
                            // refresh all selected. In LibGDX, use an event bus, because down there, that's shit
                            grid.items.forEach { (_, pn) -> if (pn.children.size == 2) pn.children.removeLast() }

                            val selected = alcHexGrid.selected
                            grid.items.filterKeys { it in selected }.values.forEach { p ->
                                p.children.add(ImageView(selectedImage).also {
                                    it.fitWidth = p.minWidth + 8
                                    it.fitHeight = p.minHeight + 8
                                })
                            }
                        }
                    }
                }

                pane.setImage(marbleImages.getValue(marbleState.marble), marbleState)
            }

        if (alcHexGrid.won) {
            Alert(Alert.AlertType.CONFIRMATION, "Vous avez gagné !").showAndWait()
        }
    }

    private fun StackPane.setImage(image: Image, marbleState: MarbleState) {
        if (marbleState.removed) {
            return
        }

        val imageView = ImageView(image)
        imageView.fitWidth = minWidth
        imageView.fitHeight = minHeight
        if (!marbleState.active) {
            imageView.opacity = 0.4
        }

        if (children.isEmpty()) {
            children.add(imageView)
        } else {
            children[0] = imageView
        }
    }

    companion object {
        private val marbleImages = Marble.values()
            .associateWith { Image(Controller::class.java.classLoader.getResourceAsStream("hexes/normal/${it.name.toLowerCase()}.png")) }

        private val hoverMarbleImages = Marble.values()
            .associateWith { Image(Controller::class.java.classLoader.getResourceAsStream("hexes/hover/${it.name.toLowerCase()}.png")) }

        private val selectedImage = Image(Controller::class.java.classLoader.getResourceAsStream("hexes/selected.png"))
    }
}
