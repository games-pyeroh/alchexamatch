package io.pyerohdc

import javafx.geometry.Point3D
import kotlin.math.sqrt

open class HexGrid<T>(items: Map<Point3D, T>) {

    companion object {
        fun sideSizeFromCollectionSize(size: Int): Int {
            return ((sqrt(1.0 + 4 * ((size - 1) / 3)) - 1) / 2.0).toInt() + 1
        }

        fun allPointsForSize(sideSize: Int): List<Point3D> {
            val sideSizeForCoordinates = sideSize - 1
            val coordinatesRange = -sideSizeForCoordinates..sideSizeForCoordinates
            return coordinatesRange
                .flatMapIndexed { index, z ->
                    val range = if (index < sideSize) {
                        -index..sideSizeForCoordinates
                    } else {
                        -sideSizeForCoordinates..(sideSize * 2 - index - 2)
                    }
                    range.zip(range.reversed())
                        .map { Point3D(it.first.toDouble(), it.second.toDouble(), z.toDouble()) }
                }
        }
    }

    private val sideSize = items.keys
        .flatMap { listOf(it.x, it.y, it.z) }
        .map { it.toInt() }
        .maxOrNull()
        ?.plus(1) ?: 0
    private val sideSizeForCoordinates = sideSize - 1

    val items: Map<Point3D, T>

    init {
        check(items.keys.all {
            it.x == it.x.toInt().toDouble() && it.y == it.y.toInt().toDouble() && it.z == it.z.toInt()
                .toDouble() && it.x + it.y + it.z == 0.0
        }) { "Invalid points : $items" }

        val coordinatesRange = -sideSizeForCoordinates.toDouble()..sideSizeForCoordinates.toDouble()
        check(
            items.keys.map { it.x }.distinct().all { it in coordinatesRange }) { "Invalid positions (x) : $items" }
        check(
            items.keys.map { it.y }.distinct().all { it in coordinatesRange }) { "Invalid positions (y) : $items" }
        check(
            items.keys.map { it.z }.distinct().all { it in coordinatesRange }) { "Invalid positions (z) : $items" }

        this.items = items.toMap()
    }

    override fun toString(): String {
        val allCoordinates = allPointsForSize(sideSize)
        return allCoordinates
            .asSequence()
            .map { it to items[it] }
            .groupBy { it.first.z }
            .asSequence()
            .sortedBy { it.key }
            .map { it.value.sortedBy { p -> p.first.x } }
            .map { it.joinToString(separator = " ") { p -> p.second?.toString() ?: "_" } }
            .mapIndexed { index, s ->
                when {
                    index == sideSizeForCoordinates -> ""
                    index < sideSizeForCoordinates -> " ".repeat(sideSizeForCoordinates - index)
                    else -> " ".repeat(index - sideSizeForCoordinates)
                } + s
            }
            .joinToString(separator = "\n")
    }

}
