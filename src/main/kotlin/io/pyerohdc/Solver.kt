package io.pyerohdc

import javafx.geometry.Point3D

class Solver(private val alcHexGrid: AlcHexGrid) {

    companion object {
        private const val maxIterations = 100000
    }

    private val alreadyMetConfigurations = mutableSetOf<String>()

    private var iterations = 0

    fun isSolvable(): Boolean {
        return alcHexGrid.isSolvable()
    }

    private fun AlcHexGrid.isSolvable(): Boolean {
        if (this.won) {
            return true
        }

        if (iterations >= maxIterations) {
            return false
        }

        // if the nextMetal is surrounded by higher rank metals and eventually by quicksilver, it's already unsolvable
        if (this.nextMetalMarble != null && !metalHasSpace(nextMetalMarble)) {
            return false
        }

        if (hasOddElements()) {
            alreadyMetConfigurations += this.toString()
            iterations++
            return false
        }

        if (this.toString() in alreadyMetConfigurations) {
            iterations++
            return false
        }

        val activeMarbles = items.filterValues { !it.removed && it.active }

        val grid = removeObviousMarbles(activeMarbles)
        if (grid !== this) {
            return grid.isSolvable()
        }

        val nextSteps = activeMarbles
            .flatMap { entry ->
                activeMarbles
                    .filterKeys { it != entry.key }
                    .filterValues { entry.value.marble.canBePairedWith(it.marble) }
                    .map { setOf(it, entry) }
            }
            .asSequence()
            .distinct()
            .sortedByDescending { it.maxOf { e -> e.value.marble } } // to match available metals first
            .map { it.map { e -> e.key } }
            .mapNotNull {
                val gridCopy = AlcHexGrid(nextMetalMarble, items)
                val l = it.toList()
                gridCopy.select(l.first())
                gridCopy.select(l.last())
            }
            .distinctBy { it.toString() }
            .toList()

        return nextSteps.any { it.isSolvable().also { b -> if (!b) alreadyMetConfigurations += it.toString() } }
            .also { if (!it) iterations++ }
    }

    private fun AlcHexGrid.removeObviousMarbles(activeMarbles: Map<Point3D, MarbleState>): AlcHexGrid {
        var grid = this

        val gold = activeMarbles.filterValues { it.marble == Marble.GOLD }
        if (gold.isNotEmpty()) {
            grid = grid.select(gold.keys.first())!!
        }

        val lifeAndDeath = items.filterValues { !it.removed && it.marble in listOf(Marble.DEATH, Marble.LIFE) }
        if (activeMarbles.entries.containsAll(lifeAndDeath.entries)) {
            grid = lifeAndDeath
                .toList()
                .partition { it.second.marble == Marble.LIFE }
                .let { it.first.zip(it.second) }
                .map { it.first.first to it.second.first }
                .fold(grid) { g, p ->
                    g.select(p.first)
                    g.select(p.second)!!
                }
        }

        for (elementalMarble in listOf(Marble.AIR, Marble.EARTH, Marble.WATER, Marble.FIRE)) {
            val elements = grid.items.filterValues { !it.removed && it.marble == elementalMarble }
            if (elements.size % 2 == 0 && activeMarbles.entries.containsAll(elements.entries)) {
                grid = elements
                    .toList()
                    .map { it.first }
                    .chunked(2)
                    .fold(grid) { g, p ->
                        g.select(p.first())
                        g.select(p.last())!!
                    }
            }
        }

        return grid
    }

    private fun AlcHexGrid.hasOddElements(): Boolean {
        val remainingElements = items.filterValues { !it.removed && it.marble.canBePairedWith(Marble.SALT) }
            .values
            .map { it.marble }
            .groupingBy { it }
            .eachCount()
        return remainingElements.getOrDefault(Marble.SALT, 0) < (remainingElements.minus(Marble.SALT)
            .filterValues { it % 2 != 0 }.count())
    }

    private fun AlcHexGrid.metalHasSpace(nextMetalMarble: Marble.MetalMarbleOrder): Boolean {
        val neighbours = neighbours(items.filterValues { it.marble == nextMetalMarble.marble }.keys.first())
        val metalNeighbours = neighbours.mapValues { entry ->
            val state = entry.value
            if (state != null && state.marble in Marble.MetalMarbleOrder.values()
                    .filter { it.order > nextMetalMarble.order }.map { it.marble } + Marble.QUICKSILVER
            ) {
                state
            } else null
        }.values.toList()

        return (metalNeighbours
            .windowed(3, partialWindows = false)
            .any { it.size == 3 && it.filterNotNull().isEmpty() }
                || metalNeighbours.rotate(2)
            .windowed(3, partialWindows = false)
            .any { it.size == 3 && it.filterNotNull().isEmpty() })
    }
}
